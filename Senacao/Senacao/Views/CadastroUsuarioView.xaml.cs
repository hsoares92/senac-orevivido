﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Senacao.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CadastroUsuarioView : ContentPage
    {
        public Usuario Usuario { get; set; }

        public CadastroUsuarioView()
        {
            InitializeComponent();
        }


        private void Button_Cadastrar(object sender, EventArgs e)
        {
           
            Console.WriteLine(this.Usuario.Nome);
            Console.WriteLine(this.Usuario.Idade);
            Console.WriteLine(this.Usuario.Telefone);
            Console.WriteLine(this.Usuario.Email);
            Console.WriteLine(this.Usuario.Senha);
            Console.WriteLine("cadastro realizado com sucesso.", "Sucesso", "OK");

            Navigation.PushAsync(new LoginView());
        }
    }
}