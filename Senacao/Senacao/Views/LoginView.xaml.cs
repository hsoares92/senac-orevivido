﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Senacao.Views
{
    public class Usuario
    {
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Nome { get; set; }
        public int Idade { get; set; }
        public string Telefone { get; set; }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginView : ContentPage
    {
        public Usuario Usuario { get; set; }
        public LoginView()
        {
            InitializeComponent();
            this.Usuario = new Usuario();
            this.BindingContext = this;
            
        }

        public async void SignIn()
        {
            Uri url = new Uri("http://10.141.46.15:3003/Login");
            HttpResponseMessage httpResponse = await Services.HttpService.PostRequest(url.AbsoluteUri, Usuario);
            if (httpResponse.IsSuccessStatusCode)
            {
                ActivityIndicatorLogin.IsRunning = false;
                await DisplayAlert("Sucesso", "Login realizado com sucesso", "Ok");
                Navigation.InsertPageBefore(new ListagemView(), this);
                await Navigation.PopAsync();
            }
            else
            {
                ActivityIndicatorLogin.IsRunning = false;
                var data = Services.HttpService.GetErrorDataFromHttpResponseMessage(httpResponse);
                Console.WriteLine(data["error"]);
                await DisplayAlert("Acesso negado", data["error"].ToString(), "OK");
            }
        }


        private void Button_Login_Clicked(object sender, EventArgs e)
        {
            Console.WriteLine("==========");
            Console.WriteLine(this.Usuario.Email);
            Console.WriteLine(this.Usuario.Senha);
            SignIn();
        }


        private void Button_Cadastrar_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CadastroUsuarioView());
        }
    }
}